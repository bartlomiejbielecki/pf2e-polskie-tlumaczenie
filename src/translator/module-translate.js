// Tłumaczenie modułu pf2e-tokens-characters
Hooks.on('setup', () => {
    if (game.modules.get('pf2e-tokens-characters')?.active) {
        fetch('modules/lang-pl-pf2e/translation/pf2e-tokens-characters/pl.json')
            .then(response => response.json())
            .then(translations => {
                game.i18n.translations = mergeObject(game.i18n.translations, translations);
            })
            .catch(err => console.error("Failed to load Polish translations to module pf2e-tokens-characters", err));
    }
});

// Tłumaczenie modułu pf2e-kingmaker
Hooks.on('setup', () => {
    if (game.modules.get('pf2e-kingmaker')?.active) {
        fetch('modules/lang-pl-pf2e/translation/pf2e-kingmaker/pl.json')
            .then(response => response.json())
            .then(translations => {
                game.i18n.translations = mergeObject(game.i18n.translations, translations);
            })
            .catch(err => console.error("Failed to load Polish translations to module pf2e-kingmaker", err));
    }
});

// Tłumaczenie modułu pf2e-decks-harrow
Hooks.on('setup', () => {
    if (game.modules.get('pf2e-decks-harrow')?.active) {
        fetch('modules/lang-pl-pf2e/translation/pf2e-decks-harrow/pl.json')
            .then(response => response.json())
            .then(translations => {
                game.i18n.translations = mergeObject(game.i18n.translations, translations);
            })
            .catch(err => console.error("Failed to load Polish translations to module pf2e-decks-harrow", err));
    }
});

// Tłumaczenie modułu starfinder-field-test-for-pf2e
Hooks.on('setup', () => {
    if (game.modules.get('starfinder-field-test-for-pf2e')?.active) {
        fetch('modules/lang-pl-pf2e/translation/starfinder-field-test-for-pf2e/pl.json')
            .then(response => response.json())
            .then(translations => {
                game.i18n.translations = mergeObject(game.i18n.translations, translations);
            })
            .catch(err => console.error("Failed to load Polish translations to module starfinder-field-test-for-pf2e", err));
    }
});

// Tłumaczenie modułu pf2e-award-xp
Hooks.on('setup', () => {
    if (game.modules.get('pf2e-award-xp')?.active) {
        fetch('modules/lang-pl-pf2e/translation/pf2e-award-xp/pl.json')
            .then(response => response.json())
            .then(translations => {
                game.i18n.translations = mergeObject(game.i18n.translations, translations);
            })
            .catch(err => console.error("Failed to load Polish translations to module pf2e-award-xp", err));
    }
});