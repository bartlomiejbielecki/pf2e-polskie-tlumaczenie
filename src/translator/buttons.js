// Przycisk "Odśwież aktora" pozwala na pobranie tłumaczeń aktorów na scenie bądź zaimportowanych
async function injectHeaderTranslationButtonActor(sheet, buttons) {
    // Sprawdzenie, czy karta nie ma klasy "actor character" lub "party"
    if (sheet.actor && sheet.actor.type === "character") return;
    if (sheet.actor && sheet.actor.type === "party") return;

    if (game.settings.get("lang-pl-pf2e", "refreshActor")) {
        const document = sheet.item ?? sheet.object;
        if (!(document instanceof foundry.abstract.Document)) throw new Error('Could not locate sheet\'s document');

        // Definiowanie przycisku "Odśwież aktora"
        const refreshButton = {
            class: 'refresh-actor',
            icon: 'fas fa-sync',
            label: "Odśwież aktora",
            onclick: _ => refreshActor(sheet.actor.id),
        };

        // Znalezienie indeksu przycisku "Zamknij"
        const closeIndex = buttons.findIndex(button => button.class.includes('close'));

        // Wstaw przycisk "Odśwież aktora" przed "Zamknij"
        if (closeIndex !== -1) {
            buttons.splice(closeIndex, 0, refreshButton);  // Wstaw przed "Zamknij"
        } else {
            buttons.push(refreshButton);  // Jeśli brak "Zamknij", dodaj na koniec
        }
    }
}

Hooks.on("getActorSheetHeaderButtons", injectHeaderTranslationButtonActor);

// Funkcja tłumaczenia aktora
async function refreshActor(actorId) {
    if (game.settings.get("lang-pl-pf2e", "refreshActor")) {
        try {
            const actor = game.actors.get(actorId);
            if (!actor) {
                ui.notifications.error(`Nie znaleziono aktora o ID ${actorId}.`);
                return;
            }

            const sourceId = actor._source._stats?.compendiumSource;
            if (!sourceId) {
                ui.notifications.error(`Aktor nie pochodzi z kompendium.`);
                return;
            }

            const parts = sourceId.split(".");
            const systemName = parts[1];
            const compendiumName = parts[2];
            const entryId = parts[4];

            const pack = game.packs.get(`${systemName}.${compendiumName}`);
            if (!pack) {
                ui.notifications.error(`Nie znaleziono kompendium dla tego aktora.`);
                return;
            }

            const entry = await pack.getDocument(entryId);
            if (!entry) {
                ui.notifications.error(`Nie można pobrać danych aktora z kompendium.`);
                return;
            }

            // Ochrona przed podmianą obrazka
            const updateData = entry.toObject();
            delete updateData.img;  // Nie aktualizuj obrazka
            delete updateData.token; // Nie aktualizuj tokena

            // Zachowaj folder aktora
            updateData.folder = actor.folder?.id || null;
            
            if (game.settings.get("lang-pl-pf2e", "refreshActorName")) {
                // Zaktualizuj nazwę tokena w prototypie
                const tokenData = actor.prototypeToken.toObject();
                tokenData.name = entry.name;
                await actor.prototypeToken.update({ name: entry.name });

                // Zaktualizuj nazwę tokena na scenie (jeśli jest już umieszczony)
                const sceneTokens = canvas.tokens.placeables.filter(t => t.actor?.id === actor.id);
                for (let token of sceneTokens) {
                    await token.document.update({ name: entry.name });
                }
            }
            else{
                delete updateData.name; // Nie aktualizuj nazwy
            }

            // Sprawdź, czy aktor już istnieje przed aktualizacją
            if (game.actors.get(actor.id)) {
                await actor.update(updateData);
                ui.notifications.info(`Aktor ${actor.name} został odświeżony z kompendium.`);
            } else {
                ui.notifications.error(`Aktor nie może być zaktualizowany, ponieważ nie istnieje.`);
            }
            
            // Aktualizacja zaklęć aktora
            for (const spell of actor.items.filter(item => item.type === "spell")) {
                const spellCompendiumSource = spell._source._stats?.compendiumSource;

                if (spellCompendiumSource) {
                    const spellParts = spellCompendiumSource.split(".");
                    const spellSystem = spellParts[1];
                    const spellCompendium = spellParts[2];
                    const spellEntryId = spellParts[4];
                    const spellPack = game.packs.get(`${spellSystem}.${spellCompendium}`);

                    if (!spellPack) {
                        console.warn(`Nie znaleziono kompendium dla zaklęcia ${spell.name}: ${spellSystem}.${spellCompendium}`);
                        continue;  // Przejdź do następnego zaklęcia
                    }

                    if (spellPack && !spellPack.index.size) {
                        await spellPack.getIndex();
                    }

                    const spellEntry = await spellPack.getDocument(spellEntryId);
                    if (spellEntry && (spell.name !== spellEntry.name || spell.system.description.value !== spellEntry.system.description.value)) {
                        await spell.update({
                            name: spellEntry.name,
                            "system.description.value": spellEntry.system.description.value
                        });
                    }
                }
            }
        } catch (error) {
            console.error(error);
            ui.notifications.error("Wystąpił błąd podczas odświeżania aktora.");
        }
    }
}

// Przycisk "Zmień Opis", który pozwala na podejrzenie angielskiego opisu
Hooks.on("getItemSheetHeaderButtons", injectHeaderTranslationButtonItem)

async function injectHeaderTranslationButtonItem(sheet, buttons) {
    if (game.settings.get("lang-pl-pf2e", "changeTranslation")) {
        const document = sheet.item ?? sheet.object;
        if (!(document instanceof foundry.abstract.Document)) throw new Error('Could not locate sheet\'s document');
        const button = {
            class: 'data-translator',
            icon: 'fa fa-language',
            label: "Zmień opis",
            onclick: _ => changeTranslation(document, buttons, sheet),
        };
        buttons.unshift(button);
    }
};

async function changeTranslation(document) {
    try {
        if (document.actor !== null) {
            const actorId = document.actor._id;
            const itemId = document.id;
            const actor = game.actors.get(actorId);
            const matchingItems = actor.items.filter(item => item.id === itemId)[0];
            const isIdentify = matchingItems.system?.identification?.status;
            if (isIdentify === "unidentified") {
                ui.notifications.warn("Przedmiot jest niezdentyfikowany");
            }
            else {
                if (actor.type === "character") {
                    if (matchingItems.flags["lang-pl-pf2e"]?.lang !== "en" || undefined) {
                        const oldDescription = matchingItems.system.description.value;
                        const compendiumsource = document._stats.compendiumSource;
                        let splitString = compendiumsource.split(".");
                        let result = splitString.slice(1, 3).join(".");
                        const url = `modules/lang-pl-pf2e/translation/en/compendium/${result}.json`;
                        const oryginalName = matchingItems.flags.babele.originalName;
                        const response = await fetch(url);
                        const data = await response.json();
                        const dataEn = data.entries
                        const resultentetis = Object.entries(dataEn).filter(([key, value]) =>
                            value.name.includes(oryginalName)
                        );
                        const filteredItems = Object.fromEntries(resultentetis);
                        const newText = filteredItems[oryginalName].description;
                        let updateData = {
                            ["system.description.value"]: newText,
                            ["system.description.old"]: oldDescription
                        };
                        matchingItems.setFlag("lang-pl-pf2e", "lang", "en");
                        const gmNote = matchingItems.system.description.gm
                        const gmNoteNew = filteredItems[oryginalName].gmNote
                        if (gmNote !== "") {
                            updateData = {
                                ["system.description.gm"]: gmNoteNew,
                                ["system.description.gmold"]: gmNote,
                                ["system.description.value"]: newText,
                                ["system.description.old"]: oldDescription
                            };
                        }
                        await matchingItems.update(updateData);
                    }
                    else {
                        const oldDescription = matchingItems.system.description.old;
                        let updateData = { ["system.description.value"]: oldDescription };
                        const gmNote = document.system.description.gm;
                        if (gmNote !== "") {
                            const gmNoteNew = document.system.description.gmold
                            updateData = {
                                ["system.description.gm"]: gmNoteNew,
                                ["system.description.value"]: oldDescription
                            }
                        }
                        matchingItems.setFlag("lang-pl-pf2e", "lang", "pl");
                        await matchingItems.update(updateData);
                    }
                }
                else {
                    if (matchingItems.flags["lang-pl-pf2e"]?.lang !== "en" || undefined) {
                        const oldDescription = matchingItems.system.description.value;
                        const compendiumsource = actor.sourceId;
                        let splitString = compendiumsource.split(".");
                        let result = splitString.slice(1, 3).join(".");
                        const url = `modules/lang-pl-pf2e/translation/en/compendium/${result}.json`;
                        const oryginalName = actor.flags.babele.originalName;
                        const response = await fetch(url);
                        const data = await response.json();
                        const dataEn = data.entries
                        const resultActor = Object.entries(dataEn).filter(([key, value]) =>
                            value.name.includes(oryginalName)
                        );
                        const filteredActor = Object.fromEntries(resultActor);
                        const url2 = `modules/lang-pl-pf2e/translation/pl/compendium/${result}.json`;
                        const response2 = await fetch(url2);
                        const data2 = await response2.json();
                        const dataPl = data2.entries
                        const resultActorPL = Object.entries(dataPl).filter(([key, value]) =>
                            value.name.includes(actor.name)
                        );
                        const filteredActorPL = Object.fromEntries(resultActorPL);
                        const filteredItemsPL = Object.entries(filteredActorPL[oryginalName].items)
                            .filter(([key, item]) => item.name === matchingItems.name)
                            .map(([key]) => key);
                        let filteredItems = Object.entries(filteredActor[oryginalName].items)
                            .filter((item) => item[0] === filteredItemsPL[0])
                        let newText = (filteredItems[0] && filteredItems[0][1] && filteredItems[0][1].description) ? filteredItems[0][1].description : "";
                        let gmNoteNew = "";
                        if (filteredItems.length === 0) {
                            const compendiumID = matchingItems.flags.core.sourceId;
                            splitString = compendiumID.split(".");
                            result = splitString.slice(1, 3).join(".");
                            const url3 = `modules/lang-pl-pf2e/translation/en/compendium/${result}.json`;
                            const oryginalName2 = matchingItems.flags.babele.originalName;
                            const response2 = await fetch(url3);
                            const data3 = await response2.json();
                            const dataEn2 = data3.entries
                            let Items2 = Object.entries(dataEn2).filter((value) =>
                                value[1].name.includes(oryginalName2)
                            );
                            const filteredItems2 = Object.fromEntries(Items2);
                            newText = filteredItems2[oryginalName2].description;
                            gmNoteNew = filteredItems2[oryginalName2].gmNote
                        }
                        else {
                            gmNoteNew = filteredItems[oryginalName].gmNote
                        }
                        let updateData = {
                            ["system.description.value"]: newText,
                            ["system.description.old"]: oldDescription
                        };
                        const gmNote = document.system.description.gm
                        if (gmNote !== "") {
                            updateData = {
                                ["system.description.gm"]: gmNoteNew,
                                ["system.description.gmold"]: gmNote,
                                ["system.description.value"]: newText,
                                ["system.description.old"]: oldDescription
                            };
                        }
                        matchingItems.setFlag("lang-pl-pf2e", "lang", "en");
                        await matchingItems.update(updateData);
                    }
                    else {
                        const oldDescription = matchingItems.system.description.old;
                        const gmNote = matchingItems.system.description.gm
                        let updateData = { ["system.description.value"]: oldDescription };
                        if (gmNote !== "") {
                            const gmNoteNew = document.system.description.gmold
                            updateData = {
                                ["system.description.gm"]: gmNoteNew,
                                ["system.description.value"]: oldDescription
                            }
                        }
                        matchingItems.setFlag("lang-pl-pf2e", "lang", "pl");
                        await matchingItems.update(updateData);
                    }
                }
            }
        }
        else {
            let oryginalName = ''
            try {
                oryginalName = document.flags.babele.originalName;
            } catch (error) {
                ui.notifications.warn("Nie można przetłumaczyć opisu!");
                return;
            }
            const oldDescription = document.system.description.value;
            const isIdentify = document.system?.identification?.status;
            let updateData = {}
            if (isIdentify === "unidentified") {
                ui.notifications.warn("Przedmiot jest niezdentyfikowany");
            }
            else {
                if (document.flags["lang-pl-pf2e"]?.lang !== "en" || undefined) {
                    const compendiumsource = document._source._stats.compendiumSource;
                    let splitString = compendiumsource.split(".");
                    let result = splitString.slice(1, 3).join(".");
                    const url = `modules/lang-pl-pf2e/translation/en/compendium/${result}.json`;
                    const response = await fetch(url);
                    const data = await response.json();
                    const dataEn = data.entries
                    const resultentetis = Object.entries(dataEn).filter(([key, value]) =>
                        value.name.includes(oryginalName)
                    );
                    const filteredItems = Object.fromEntries(resultentetis);
                    const newText = filteredItems[oryginalName].description;
                    updateData = {
                        ["system.description.value"]: newText,
                        ["system.description.old"]: oldDescription
                    };
                    const gmNote = document.system.description.gm
                    const gmNoteNew = filteredItems[oryginalName].gmNote
                    if (gmNote !== "") {
                        updateData = {
                            ["system.description.gm"]: gmNoteNew,
                            ["system.description.gmold"]: gmNote,
                            ["system.description.value"]: newText,
                            ["system.description.old"]: oldDescription
                        };
                    }
                    document.setFlag("lang-pl-pf2e", "lang", "en");
                    await document.update(updateData);
                }
                else {
                    const oldDescription = document.system.description.old;
                    const gmNote = document.system.description.gm
                    updateData = { ["system.description.value"]: oldDescription };
                    document.setFlag("lang-pl-pf2e", "lang", "pl");
                    if (gmNote !== "") {
                        const gmNoteNew = document.system.description.gmold
                        updateData = {
                            ["system.description.gm"]: gmNoteNew,
                            ["system.description.value"]: oldDescription
                        }
                    }
                    await document.update(updateData);
                }
            }
        }
    } catch (error) {
        ui.notifications.error("Nie można przetłumaczyć przedmiotu z kompendium!");
        return;
    }
}