// Wpisuje angielską nazwę pod polską nazwą w kartach 
Hooks.on(`renderItemSheetPF2e`, async (document, html) => {
    await addoryginalname(document, html)
});

Hooks.on(`renderNPCSheetPF2e`, (document, html) => {
    addoryginalname2(document, html)
});

Hooks.on(`renderHazardSheetPF2e`, (document, html) => {
    addoryginalname3(document, html)
});

Hooks.on(`renderVehicleSheetPF2e`, (document, html) => {
    addoryginalname4(document, html)
});

async function addoryginalname(document, html) {
    if (game.settings.get("lang-pl-pf2e", "dual-language-names")) {
        if (document.document.flags.babele && document.document.flags.babele.originalName) {
            const originalname = document.document.flags.babele.originalName;
            const translatedname = html[0].querySelector('input[name="name"]').value;
            const translatednamehtml = html[0].querySelector('input[name="name"]');
            const action = html[0].querySelector('span.action-glyph');
            if (originalname !== translatedname) {
                const detailElement = html[0].querySelector('.details');
                const levelElement = html[0].querySelector('.level');
                const detailsDivhtml = translatednamehtml?.outerHTML;
                const engnamehtml = `
                   <div style="flex:1;">       
                        ${detailsDivhtml}
                       <div class="english-name">
                           <span>${originalname}</span>     
                       </div>
                   </div>`;
                translatednamehtml.remove();
                detailElement.insertAdjacentHTML(`afterbegin`, engnamehtml);
                if (levelElement) {
                    if (action !== null) {
                        action.remove();

                        levelElement.insertAdjacentHTML(`beforeend`, action.outerHTML);
                    }
                }
            }
        } else {
            console.log("Zmienna originalname nie istnieje lub ma wartość undefined, null lub pustą.");
        }
    }
}

function addoryginalname2(document, html) {
    if (game.settings.get("lang-pl-pf2e", "dual-language-names")) {
        if (document.document.flags.babele && document.document.flags.babele.originalName) {
            const originalname = document.document.flags.babele.originalName;
            const translatedname = html[0].querySelector('input[name="name"]').value;
            if (originalname !== translatedname) {
                const engnamehtml = `
                       <div class="english-name-other">
                           <span>${originalname}</span>     
                       </div>`;
                const detailsDiv3 = html[0]?.querySelector('.image-container');
                detailsDiv3.insertAdjacentHTML(`afterend`, engnamehtml);
            }
        } else {
            console.log("Zmienna originalname nie istnieje lub ma wartość undefined, null lub pustą.");
        }
    }
}

function addoryginalname3(document, html) {
    if (game.settings.get("lang-pl-pf2e", "dual-language-names")) {
        if (document.document.flags.babele && document.document.flags.babele.originalName) {
            const originalname = document.document.flags.babele.originalName;
            const translatedname = html[0].querySelector('input[name="name"]').value;
            if (originalname !== translatedname) {
                const engnamehtml = `
                       <section class="english-name-other">
                           <span>${originalname}</span>     
                       </section>`;
                const detailsDiv3 = html[0]?.querySelector('.sidebar');
                detailsDiv3.insertAdjacentHTML(`afterbegin`, engnamehtml);
            }
        } else {
            console.log("Zmienna originalname nie istnieje lub ma wartość undefined, null lub pustą.");
        }
    }
}

function addoryginalname4(document, html) {
    if (game.settings.get("lang-pl-pf2e", "dual-language-names")) {
        if (document.document.flags.babele && document.document.flags.babele.originalName) {
            const originalname = document.document.flags.babele.originalName;
            const translatedname = html[0].querySelector('input[name="name"]').value;
            if (originalname !== translatedname) {
                const engnamehtml = `
                       <div class="english-name-vehicle">
                           <span>${originalname}</span>     
                       </div>`;
                const detailsDiv3 = html[0]?.querySelector('.char-name');
                console.log(detailsDiv3);
                detailsDiv3.insertAdjacentHTML(`afterend`, engnamehtml);
            }
        } else {
            console.log("Zmienna originalname nie istnieje lub ma wartość undefined, null lub pustą.");
        }
    }
}