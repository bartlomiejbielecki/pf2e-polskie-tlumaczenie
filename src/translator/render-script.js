Hooks.on('renderChatMessage', (message, html, data) => {
    try {
        if (!html[0] || !html[0].parentNode) return;

        // Znalezienie odpowiedniego div'a
        let resultDiv = html.find('div.result.degree-of-success');

        // Znalezienie spanu wewnątrz tego div'a i zamiana tekstu
        let spans = resultDiv.find('span[data-whose="opposer"]');
        spans.each(function () {
            let spanText = $(this).text();
            $(this).text(spanText.replace('by ', 'o '));
        });

        // ============================================================
        // Zamiana frazy "To Recall Knowledge, roll:"
        let element = html[0].querySelector('.message-content');
        if (element && element.innerHTML.includes('To Recall Knowledge, roll:')) {
            element.innerHTML = element.innerHTML.replace('To Recall Knowledge, roll:', 'Rzuć na Przywołanie Wiedzy: ');
        }

        // ============================================================
        // Zamiana "was randomly selected."
        const targetMessage = html[0].querySelector('.random-target-message-target');
        if (targetMessage) {
            const parent = targetMessage.parentNode;
            if (parent) {
                parent.innerHTML = parent.innerHTML.replace('was randomly selected.', 'został wybrany losowo.');
            }
        }

        // ============================================================
        // Zamiana "The pool of candidates for this selection:"
        const poolMessage = html[0].querySelector('.dice-tooltip section');
        if (poolMessage) {
            poolMessage.innerHTML = poolMessage.innerHTML.replace('The pool of candidates for this selection:', 'Pula kandydatów do tej selekcji:');
        }

        // ============================================================
        // Znalezienie sekcji persistent
        let persistentSection = html.find('section.persistent h4');

        // Sprawdź czy sekcja istnieje i zamień tekst
        if (persistentSection.length > 0) {
            persistentSection.each(function () {
                let heading = $(this);
                if (heading.text() === "Persistent Damage") {
                    heading.text("Trwałe Obrażenia");
                }
            });
        }
        // ============================================================

        // ============================================================
        let flavorTextElement = html.find('header.message-header .flavor-text');

        // Sprawdź, czy element istnieje
        if (flavorTextElement.length > 0) {
            let content_actions = flavorTextElement.html();

            // Zastępowanie tekstu
            content_actions = content_actions.replace('has', 'ma')
                .replace('2 actions remaining', '2 pozostałe akcje')
                .replace('1 actions remaining', '1 pozostałą akcję')
                .replace('0 actions remaining', '0 pozostałych akcji');

            flavorTextElement.html(content_actions);
        }

        // ============================================================
        let messageContentElement = html.find('.message-content');

        // Sprawdź, czy element istnieje
        if (messageContentElement.length > 0) {
            let content = messageContentElement.html();

            // Sprawdź, czy istnieje słowo "Distributed"
            if (content.includes('Distributed')) {
                // Zastępowanie tekstu według podanych reguł
                content = content.replace('Distributed', 'Rozdzielono')
                    .replace('from', 'ze schowka')
                    .replace('to', 'dla')
                    .replace('each', 'każde')
                    .replace('and', 'i')
                    .replace('gp', 'sz')
                    .replace('sp', 'ss')
                    .replace('cp', 'sm')
                    .replace('pp', 'sp');
            }
            // ============================================================
            // Dodanie zamiany słów: spell, arcane, etc.
            // const translationMap = {
            //     "spell": "zaklęcie",
            //     "arcane": "Tajemny",
            //     "divine": "Boski",
            //     "occult": "Okultystyczny",
            //     "primal": "Pierwotny"
            // };
            // const regex = new RegExp(`\\b(${Object.keys(translationMap).join("|")})\\b`, "gi");
            // content = content.replace(regex, (matched) => translationMap[matched.toLowerCase()]);

            // Aktualizacja HTML
            messageContentElement.html(content);
        }

        // ============================================================
        // Zamiana "targeted" na "stał się celem ataku"
        const notificationItems = document.querySelectorAll('.notification.info');
        notificationItems.forEach(item => {
            if (item.innerHTML.includes('targeted')) {
                item.innerHTML = item.innerHTML.replace('targeted', 'stał się celem ataku');
            }
        });
    } catch (error) {
        console.error("Błąd w hooku renderChatMessage: ", error);
    }
});


// Konwersja monet
Hooks.on('renderApplication', (app, html, data) => {
    let coinLabel = html.find('label.break-coins span');

    // Sprawdź, czy etykieta istnieje i zmień jej tekst
    if (coinLabel.length > 0) {
        coinLabel.text("Zezwalaj na konwersję typów monet");
    }
});

// Przyciski Forge na zakładce Ustawienia
Hooks.on('renderPlayerList', (app, html, data) => {
    // Tłumaczenie "Join Game As"
    const joinButton = html.find('button[data-action="join-as"]');
    if (joinButton.length) {
        joinButton.html('<i class="fas fa-random"></i> Dołącz jako...');
    }

    // Tłumaczenie "Back to The Forge"
    const forgeButton = html.find('button[data-action="forgevtt"]');
    if (forgeButton.length) {
        forgeButton.html('<i class="fas fa-home"></i> Powrót do Forge');
    }
});

// Okno Dołącz jako tymczasowy gracz
Hooks.on('renderJoinGameDialog', (app, html, data) => {
    // Tłumaczenie "Join Game As"
    const title = html.find('h4.window-title');
    if (title.length) {
        title.text("Dołącz jako");
    }

    // Tłumaczenie "Select a player to re-join the game as:"
    const selectText = html.find('.dialog-content p');
    if (selectText.length) {
        selectText.text("Wybierz gracza, aby dołączyć tymczasowo jako gracz do gry:");
    }

    // Tłumaczenie "As Temporary Player"
    const buttons = html.find('button[data-join-as="temp"]');
    buttons.each(function () {
        let buttonText = $(this).text();
        $(this).text(buttonText.replace("As Temporary Player", "Jako tymczasowy gracz"));
    });
});

// ============================================================
// Ścieżka do nowej ikony dla kart typu Party
const newIconPath = "modules/lang-pl-pf2e/static/party.png";
const defaultPartyIconPath = "systems/pf2e/icons/default-icons/party.svg";

// Funkcja do zmiany ikony dla aktorów typu "party" z domyślną ikoną
async function changePartyIcons() {
    // Iterujemy przez wszystkich aktorów
    const actors = game.actors.contents;

    for (const actor of actors) {
        // Sprawdzamy, czy aktor ma typ "party" i domyślną ikonę
        if (actor.type === "party" && actor.img === defaultPartyIconPath) {
            // Ustawiamy nową ikonę
            await actor.update({ "img": newIconPath });
            console.log(`Zmieniono ikonę dla aktora typu "party": ${actor.name}`);
        }
    }
}

// Funkcja zmieniająca ikonę dla nowo utworzonych aktorów typu "party"
Hooks.on('createActor', async (actor) => {
    // Sprawdzamy, czy nowo utworzony aktor ma typ "party" i domyślną ikonę
    if (actor.type === "party" && actor.img === defaultPartyIconPath) {
        await actor.update({ "img": newIconPath });
        console.log(`Zmieniono ikonę dla nowo utworzonego aktora typu "party": ${actor.name}`);
    }
});

// Uruchamiamy funkcję zmieniającą ikony przy załadowaniu
Hooks.on('ready', () => {
    // Funkcja dodająca styl CSS do dokumentu
    function injectCSS() {
      const existingStyle = document.getElementById('custom-pause-styles');
      if (!existingStyle) {
        const style = document.createElement('style');
        style.id = 'custom-pause-styles';
        style.textContent = `
          #pause img.fa-spin {
            opacity: 0.5 !important;
            content: url('modules/lang-pl-pf2e/static/pathfinder_out.svg') !important;
          }
  
          #pause img.fa-icon {
            opacity: 0.9 !important;    
            fill: rgb(0, 42, 23);
            height: 96px;
            width: 96px;
            position: absolute; /* Aby stylizacja lewego górnego rogu działała */
            left: calc(50% - 48px);
            top: -2px;
          }
        `;
        document.head.appendChild(style);
      }
    }
  
    // Funkcja usuwająca styl CSS z dokumentu
    function removeCSS() {
      const existingStyle = document.getElementById('custom-pause-styles');
      if (existingStyle) existingStyle.remove();
    }
  
    // Funkcja dodająca obrazek
    function addCustomImage() {
      const pauseFigure = document.querySelector('figure#pause');
      if (pauseFigure && !pauseFigure.querySelector('img.fa-icon')) {
        // Tworzymy nowy znacznik img
        const newImg = document.createElement('img');
        newImg.src = "modules/lang-pl-pf2e/static/pathfinder_icon.svg"; // Nowa ścieżka
        newImg.classList.add('fa-icon');
  
        // Dodaj nowy obrazek na początek elementu <figure>
        pauseFigure.prepend(newImg);
      }
    }
  
    // Funkcja usuwająca obrazek
    function removeCustomImage() {
      const pauseFigure = document.querySelector('figure#pause');
      if (pauseFigure) {
        const customImg = pauseFigure.querySelector('img.fa-icon');
        if (customImg) customImg.remove();
      }
    }
  
    // Obserwator zmian w `#pause`
    const observer = new MutationObserver((mutationsList) => {
      for (const mutation of mutationsList) {
        if (mutation.type === 'childList' || mutation.type === 'attributes') {
          // Jeśli zmienił się `#pause`, próbuj dodać obrazek
          addCustomImage();
        }
      }
    });
  
    // Funkcja sprawdzająca ustawienie i aktywująca/dezaktywująca mechanizm
    function initializeCustomPauseFeature() {
      const isCustomPauseEnabled = game.settings.get("lang-pl-pf2e", "custom-pause");
      if (isCustomPauseEnabled) {
        injectCSS();
        addCustomImage();
        observer.observe(document.body, { childList: true, subtree: true });
      } else {
        observer.disconnect();
        removeCSS();
        removeCustomImage();
      }
    }
  
    // Zarejestruj listener na zmianę ustawień
    Hooks.on('updateSetting', (setting) => {
      if (setting.key === "lang-pl-pf2e.custom-pause") {
        initializeCustomPauseFeature();
      }
    });
  
    // Uruchom funkcję po załadowaniu
    initializeCustomPauseFeature();
  });