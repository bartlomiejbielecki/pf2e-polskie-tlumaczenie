Hooks.once("init", () => {
    if (typeof Babele !== "undefined") {
        // Ustawienia
        game.settings.register("lang-pl-pf2e", "dual-language-names", {
            name: "Wyświetl nazwy po polsku i angielsku",
            hint: 'Oprócz nazwy polskiej wyświetlaj nazwę oryginalną (o ile się różni) poniżej dla wszystkich elementów typu "rzecz".',
            scope: "world",
            type: Boolean,
            default: true,
            config: true,
        });
        game.settings.register("lang-pl-pf2e", "refreshActor", {
            name: "Wyświetl przycisk odświeżenia aktora",
            hint: 'Wyświetla na kartach NPC przycisk, z możliwością odświeżenia aktora i pobrania polskiego tłumaczenia z kompendium.',
            scope: "world",
            type: Boolean,
            default: true,
            config: true,
        });
        game.settings.register("lang-pl-pf2e", "refreshActorName", {
            name: "Odświeżanie nazwy aktora",
            hint: 'Skonfiguruj, żeby odświeżyć również nazwę aktora podczas odświeżania aktora.',
            scope: "world",
            type: Boolean,
            default: false,
            config: true,
        });
        game.settings.register("lang-pl-pf2e", "changeTranslation", {
            name: "Wyświetl przycisk oświeżenia opisu",
            hint: 'Wyświetla na kartach przedmiotów przycisk, który pozwala podejrzeć oryginalną treść opisu.',
            scope: "world",
            type: Boolean,
            default: true,
            config: true,
        });
        game.settings.register("lang-pl-pf2e", "custom-pause", {
            name: "Wyświetl niestandardowy obrazek pauzy",
            hint: 'Wyświetla niestandardowy obrazek wstrzymania gry. UWAGA! Moduły zmieniające obrazek pauzy mogą nie działać poprawnie.',
            scope: "world",
            type: Boolean,
            default: false,
            config: true,
        });
        game.settings.register("lang-pl-pf2e", "first-turn", {
            name: "Wyświetl efekt podczas pierwszer rundy walki",
            hint: 'Wyświetla efekt o braku możliwości użycia reakcji podczas pierwszej rundy walki.',
            scope: "world",
            type: Boolean,
            default: false,
            config: true,
        });
        game.babele.register({
            module: "lang-pl-pf2e",
            lang: "pl",
            dir: "translation/en/compendium",
        });
        game.babele.register({
            module: "lang-pl-pf2e",
            lang: "pl",
            dir: "translation/pl/compendium",
        });


        game.babele.registerConverters({
            translateActorDescription: (data, translation) => {
                return game.langPlPf2e.translateActorDescription(data, translation);
            },
            translateActorItems: (data, translation) => {
                return game.langPlPf2e.translateItems(data, translation, true);
            },
            // Obecnie nieużywane
            // translateActorItems: (data, translation, dataObject, translatedCompendium, translationObject) => {
            //     return game.langPlPf2e.translateActorItems(
            //         data,
            //         translation,
            //         dataObject,
            //         translatedCompendium,
            //         translationObject
            //     );
            // },
            translateDuration: (data) => {
                return game.langPlPf2e.translateValue("duration", data);
            },
            translateHeightening: (data, translation) => {
                return game.langPlPf2e.dynamicObjectListMerge(
                    data,
                    translation,
                    game.langPlPf2e.getMapping("heightening", true)
                );
            },
            translateRules: (data, translation) => {
                return game.langPlPf2e.translateRules(data, translation);
            },
            translateSource: (data) => {
                return game.langPlPf2e.translateValue("source", data);
            },
            translateTime: (data) => {
                return game.langPlPf2e.translateValue("time", data);
            },
            translateTokenName: (data, translation, _dataObject, _translatedCompendium, translationObject) => {
                return game.langPlPf2e.translateTokenName(data, translation, translationObject);
            },
        });
    }
});

// Hook nasłuchujący na rozpoczęcie walki
Hooks.on("combatStart", async (combat) => {
    if (game.settings.get("lang-pl-pf2e", "refreshActor")) {
        // Pobierz listę uczestników posortowaną według inicjatywy
        const sortedCombatants = combat.combatants.contents.sort((a, b) => b.initiative - a.initiative);

        // Znajdź pierwszego uczestnika
        const firstCombatant = sortedCombatants[0];

        // Dane efektu na podstawie JSON-a
        const effectData = {
            name: "Przed turą",
            type: "effect",
            system: {
                description: {
                    value: "<p>Nie możesz użyć reakcji, do momentu rozpoczęcia Twojej tury.</p>"
                },
                level: { value: 1 },
                duration: {
                    value: 0,
                    unit: "rounds",
                    expiry: "turn-start",
                    sustained: false
                },
                start: { value: 0, initiative: null },
                tokenIcon: { show: true }
            },
            img: "icons/svg/cancel.svg", // Ikona efektu
            flags: {
                exportSource: {
                    world: "pf2e",
                    system: "pf2e",
                    coreVersion: "12.331",
                    systemVersion: "6.7.0"
                }
            }
        };

        // Iteruj przez uczestników walki, pomijając pierwszego
        for (const combatant of sortedCombatants) {
            if (combatant === firstCombatant) continue; // Pomijamy pierwszego w kolejności

            const actor = combatant.actor;
            if (actor) {
                await actor.createEmbeddedDocuments("Item", [effectData]);
            }
        }
    }
});