# Pathfinder 2e - Polskie tłumaczenie
<div align="center">
<img src="https://gitlab.com/bartlomiejbielecki/pf2e-polskie-tlumaczenie/-/raw/642ce4f2568930bdcd9f4d763a0355b9fdad98ff/cover.webp" width="1500" alt="logo Polskiego tłumaczenia">
</div>
Jest to nieoficjalne tłumaczenie modułu PF2e na Foundry.

## Informacje ogólne
Moduł na na celu przetłumaczenie na język polski wszystkich elementów systemu Pathfinder 2e.

## Instalacja
Jeśli nie możesz wyszukać modułu w menedżerze modułów Foundry, skopiuj to łącze i użyj go w menedżerze modułów Foundry, aby zainstalować moduł.

https://gitlab.com/bartlomiejbielecki/pf2e-polskie-tlumaczenie/-/raw/main/module.json

Zainstaluj moduł Babele w menedżerze modułów Foundry.

## Chcesz pomóc?
Jeżeli jesteście zainteresowani pomocą w dalszym tłumaczeniu systemy zapraszamy na [Transifiex](https://app.transifex.com/foundry_pl/pf2e-pl/dashboard/) gdzie tłumaczymy pliki z Foundry. 

Dodatkowo o tłumaczeniu (i innych rzeczach związanych z PF2e i ogólne RPG) rozmawiamy na **discordzie** [K20 Podcast](https://discord.com/channels/628136069031919616/1282949592866095272)

## Przetłumaczone kompendia modułów
Poniżej lista przetłumaczonych kompendium modułów:

- [PF2e Action Support](https://foundryvtt.com/packages/pf2e-action-support/)
- [PF2e Ranged Combat](https://foundryvtt.com/packages/pf2e-ranged-combat/)
- [PF2e Reaction Checker](https://foundryvtt.com/packages/pf2e-reaction/)
- [PF2e Exploit Vulnerability](https://foundryvtt.com/packages/pf2e-thaum-vuln/)
- [PF2E Exploration Effects](https://foundryvtt.com/packages/pf2e-exploration-effects/)
- [PF2e Item Activations](https://foundryvtt.com/packages/pf2e-item-activations/)
- [Elemental Ammunition for Pf2e](https://foundryvtt.com/packages/elemental-ammunition-for-pf2e/)
- [PF2e - Kris's Compendium of Trade Goods](https://foundryvtt.com/packages/kctg-2e/)
- [PF2e Companion Compendia](https://foundryvtt.com/packages/pf2e-animal-companions)
- [PF2e Macros](https://foundryvtt.com/packages/pf2e-macros)
- [PF2e Kineticist's Companion](https://foundryvtt.com/packages/pf2e-kineticists-companion)
- [PF2e Specific Familiars](https://foundryvtt.com/packages/pf2e-specific-familiars)
- [PF2e Assistant](https://foundryvtt.com/packages/pf2e-assistant)
